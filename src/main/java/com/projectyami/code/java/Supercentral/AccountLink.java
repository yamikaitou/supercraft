package com.projectyami.code.java.Supercentral;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import net.md_5.bungee.api.ChatColor;

import org.apache.commons.lang.RandomStringUtils;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class AccountLink implements Listener, CommandExecutor
{
	protected static Connection connection;
    
	public void openConnection() throws SQLException, ClassNotFoundException, IOException
	{
        if (connection != null && !connection.isClosed())
        {
            return;
        }
        
        Class.forName("com.mysql.jdbc.Driver");
        connection = DriverManager.getConnection("jdbc:mysql://"
                + Supercraft.config.getString("host") + ":" + Supercraft.config.getString("port") + "/" + Supercraft.config.getString("db"),
                Supercraft.config.getString("user"), Supercraft.config.getString("pass"));
    }
    
	@EventHandler
    public void onPlayerJoin(PlayerJoinEvent event)
    {
		event.getPlayer().sendMessage(ChatColor.MAGIC + "SC" + ChatColor.GOLD + " Type " + ChatColor.BLUE + "/linkaccount" + ChatColor.GOLD + " to link to a Forum Account");
    }

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
	{
		try
        {
			openConnection();
			
			if (cmd.getName().equalsIgnoreCase("linkaccount"))
			{
				if (!(sender instanceof Player))
				{
					sender.sendMessage("You must be a player!");
					return false;
				}
				Player player = (Player) sender;
				
				if (!(player.isOnline()))
				{
					sender.sendMessage("You must be in Online mode!");
					return false;
				}
				
				String code = null;
				Statement query = null;
				ResultSet results = null;
				
				try
				{
					do
					{
						code = RandomStringUtils.randomAlphanumeric(6);
						query = connection.createStatement();
						results = query.executeQuery("SELECT * FROM accountlink WHERE code = '" + code + "'");
					}
					while (results.getRow() != 0);
					
					query.executeUpdate("INSERT INTO accountlink (mcuuid, code) VALUES ('"+ player.getUniqueId().toString() +"', '" + code + "')");
				}
				catch (SQLException e)
				{
					e.printStackTrace();
				}
				
				player.sendMessage(ChatColor.DARK_PURPLE + "-----------------------------------------------------\n");
				player.sendMessage(ChatColor.GOLD + "" + ChatColor.MAGIC + "SC");
				player.sendMessage(ChatColor.AQUA + "Click the below link to link your account");
				player.sendMessage(ChatColor.LIGHT_PURPLE + "http://supercentral.co/link.php?mc=" + code);
				player.sendMessage(ChatColor.GOLD + "" + ChatColor.MAGIC + "SC");
				player.sendMessage(ChatColor.DARK_PURPLE + "-----------------------------------------------------\n");
				
				connection.close();
				
				return true;
			}
        }
        catch (ClassNotFoundException e)
        {
            e.printStackTrace();
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        catch (IOException e)
        {
			e.printStackTrace();
		}
		
		
		return false;
	}
}
