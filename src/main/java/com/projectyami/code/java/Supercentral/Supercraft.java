package com.projectyami.code.java.Supercentral;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

public class Supercraft extends JavaPlugin
{
    protected static FileConfiguration config;
    
	@Override
	public void onEnable()
	{
		config = this.getConfig();
		
		this.getServer().getPluginManager().registerEvents(new AccountLink(), this);
        this.getCommand("linkaccount").setExecutor(new AccountLink());
        new SelfUpdate(this).runTaskTimer(this, 36000, 36000);
	}
}
