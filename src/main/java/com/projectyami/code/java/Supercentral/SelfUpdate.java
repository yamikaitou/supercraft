package com.projectyami.code.java.Supercentral;

import java.io.BufferedReader;
import java.io.BufferedInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.logging.Level;

import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

public class SelfUpdate extends BukkitRunnable
{
	private String dlurl = "https://drone.io/bitbucket.org/yamikaitou/supercraft/files/target/Supercentral.jar";
	//private String dlurl = "http://supercentral.co/supercraft.jar";
	private String vurl = "http://supercentral.co/supercraft.txt";
	private JavaPlugin plugin;
	
	public SelfUpdate(JavaPlugin plugin)
	{
		this.plugin = plugin;
	}
	
	@Override
	public void run()
	{
		try
		{
			URL url = new URL(vurl);
            URLConnection conn = url.openConnection();
            conn.setReadTimeout(5000);
            conn.addRequestProperty("User-Agent", "Supercraft Update Checker");
            conn.setDoOutput(true);
            final BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            final String response = reader.readLine();
            
			if (Integer.parseInt(response) > 
			Integer.parseInt(plugin.getDescription().getVersion()))
			{
				plugin.getServer().getLogger().log(Level.INFO, "[SuperCraft] Update " + response + " available");
				
				if (plugin.getServer().getOnlinePlayers().size() == 0)
				{
					plugin.getServer().getLogger().log(Level.INFO, "[SuperCraft] No players online, proceeding to SelfUpdate. Server will reboot when complete.");
					this.update();
				}
			}
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}
	
	public void update()
	{
		
		try
		{
			URL download = new URL(dlurl);
		 	BufferedInputStream in = null;
		    FileOutputStream fout = null;
		    try
		    {
		        in = new BufferedInputStream(download.openStream());
		        fout = new FileOutputStream("plugins/Supercentral.jar");

		        final byte data[] = new byte[1024];
		        int count;
		        while ((count = in.read(data, 0, 1024)) != -1)
		        {
		            fout.write(data, 0, count);
		        }
		    }
		    finally
		    {
		        if (in != null)
		        {
		            in.close();
		        }
		        if (fout != null)
		        {
		            fout.close();
		        }
		    }
			
		    plugin.getServer().spigot().restart();
		    plugin.getServer().shutdown();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		
		/*
		try
		{
			
			URL url = new URL(dlurl);
            URLConnection conn = url.openConnection();
            conn.setReadTimeout(5000);
            conn.addRequestProperty("User-Agent", "Supercraft Update Downloader");
            conn.setDoOutput(true);
            Files.copy(conn.getInputStream(), Paths.get(SelfUpdate.class.getProtectionDomain().getCodeSource().getLocation().toURI()), StandardCopyOption.REPLACE_EXISTING);
            
			//FileUtils.copyURLToFile(new URL(dlurl), new File(SelfUpdate.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath()), 60, 30);
			Bukkit.getServer().reload();
		}
		catch (IOException | URISyntaxException e)
		{
			e.printStackTrace();
		}
		*/
	}
}