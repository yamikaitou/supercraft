package com.projectyami.code.java.Supercentral;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class ReadProp
{
	InputStream inputStream;
 
	public Map<String, String> getConfig() throws IOException
	{
		Map<String, String> result = new HashMap<>();
		try
		{
			Properties prop = new Properties();
			String propFileName = "config.properties";
 
			inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);
 
			if (inputStream != null)
			{
				prop.load(inputStream);
			}
			else
			{
				throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
			}
 
			result.put("host", prop.getProperty("hostname"));
			result.put("port", prop.getProperty("port"));
			result.put("user", prop.getProperty("username"));
			result.put("pass", prop.getProperty("password"));
			result.put("db", prop.getProperty("database"));
		}
		catch (Exception e)
		{
			System.out.println("Exception: " + e);
		}
		finally
		{
			inputStream.close();
		}
		
		return result;
	}
}
